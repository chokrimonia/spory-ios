//
//  MapTableViewCell.swift
//  ApplicationSpory
//
//  Created by macbook pro on 18/04/2018.
//  Copyright © 2018 macbook pro. All rights reserved.
//

import UIKit
import MapKit
class MapTableViewCell: UITableViewCell ,UISearchBarDelegate{

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var mapView: MKMapView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func searchBarShouldEndEditing(_ searchBar: UISearchBar) -> Bool {
         self.mapView.endEditing(true)
        return false
    }
 
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
