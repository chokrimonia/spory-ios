//
//  PersonnesTableViewCell.swift
//  ApplicationSpory
//
//  Created by macbook pro on 19/04/2018.
//  Copyright © 2018 macbook pro. All rights reserved.
//

import UIKit

class PersonnesTableViewCell: UITableViewCell {

    @IBOutlet weak var nomLbl: UILabel!
    @IBOutlet weak var personnesImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
