//
//  DetMesPersViewController.swift
//  ApplicationSpory
//
//  Created by macbook pro on 21/04/2018.
//  Copyright © 2018 macbook pro. All rights reserved.
//

import UIKit

class DetMesPersViewController: UIViewController ,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate{
    let  nompers = ["Me" ,"Pseudo1"]
    let nomgrps = ["Pseudo1","Pseudo2"]
    let personnes = ["female","male"]
    var isPersone = false
    // let groupes = ["rencontre4","rencontre2","rencontre2","rencontre3","rencontre2","rencontre4"]
    @IBOutlet weak var ecriremsgTextField: UITextField!
    @IBOutlet weak var mySegmented: UISegmentedControl!
    @IBOutlet weak var detMesPerTableView: UITableView!
    
        
    @IBOutlet weak var BottomView: UIView!
    
    
    override func viewDidLoad() {
           super.viewDidLoad()
     // ecriremsgTextField.layer.cornerRadius = 20
        detMesPerTableView.delegate = self
        detMesPerTableView.dataSource = self
        
        detMesPerTableView.register(UINib(nibName: "PersonnesTableViewCell", bundle: nil), forCellReuseIdentifier: "PersonnesTableViewCell")
     
        
        // Do any additional setup after loading the view.
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    //**********trois methodes***********************
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var returnValue = 0
        
        if isPersone {
            returnValue = personnes.count
        }else{
            returnValue = personnes.count
        }
        
        
        return returnValue
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = detMesPerTableView.dequeueReusableCell(withIdentifier: "PersonnesTableViewCell", for: indexPath) as! PersonnesTableViewCell
        
        
//        switch (mySegmented.selectedSegmentIndex){
//        case 0:
//            cell.personnesImage.image = UIImage(named: personnes [indexPath.row] )
//           // cell.nomLbl.text = nompers[indexPath.row]
//
//        default:
//            break
//        }
        
        if isPersone {
           cell.nomLbl.text = nompers[indexPath.row]
        }else{
           cell.nomLbl.text = nomgrps[indexPath.row]
            
        }
      return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    
    @IBAction func SegmentedControlAction(_ sender: Any) {
        detMesPerTableView.reloadData()
        
    }
    
    @IBAction func BackButtonPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated:true)
    }
    
}
