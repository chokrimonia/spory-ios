//
//  InscriptionViewController.swift
//  ApplicationSpory
//
//  Created by macbook pro on 17/04/2018.
//  Copyright © 2018 macbook pro. All rights reserved.
//

import UIKit

class InscriptionViewController: UIViewController ,UITextFieldDelegate{

    @IBOutlet weak var retaperMdpTextField: UITextField!
    @IBOutlet weak var nomTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var prenomTextField: UITextField!
    @IBOutlet weak var mdpTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
       nomTextField.layer.cornerRadius = 20
        prenomTextField.layer.cornerRadius = 20
        emailTextField.layer.cornerRadius = 20
        mdpTextField.layer.cornerRadius = 20
        retaperMdpTextField.layer.cornerRadius = 20
        
        
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
   //BUTTON BACKLOGIN
    

    @IBAction func BackButtonPressed(_ sender: Any) {
        /*
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        
        let LoginViewController = storyBoard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        self.present(LoginViewController, animated:true, completion:nil)
 */
        self.navigationController?.popViewController(animated:true)
    }
    //button s'inscrire
    
    @IBAction func SinscrireButtonPressed(_ sender: Any) {
        /*
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        
        let RechPartieViewController = storyBoard.instantiateViewController(withIdentifier: "RechPartieViewController") as! RechPartieViewController
        self.present(RechPartieViewController, animated:true, completion:nil)
 */
        let VC : RechPartieViewController = self.storyboard?.instantiateViewController(withIdentifier: "RechPartieViewController") as! RechPartieViewController
        self.navigationController?.pushViewController(VC, animated: true)
        //self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
}
