//
//  RechPartieLancerViewController.swift
//  ApplicationSpory
//
//  Created by macbook pro on 18/04/2018.
//  Copyright © 2018 macbook pro. All rights reserved.
//

import UIKit

class RechPartieLancerViewController: UIViewController,UITextFieldDelegate, SSASideMenuDelegate {

    @IBOutlet weak var moisTextField: UITextField!
    @IBOutlet weak var jourTextField: UITextField!
    @IBOutlet weak var villeTextField: UITextField!
    @IBOutlet weak var heureTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
       moisTextField.layer.cornerRadius = 20
        jourTextField.layer.cornerRadius = 20
        villeTextField.layer.cornerRadius = 20
        heureTextField.layer.cornerRadius = 20
       
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    @IBAction func BackButtonPressed(_ sender: Any){
       
      self.navigationController?.popViewController(animated:true)
    }
    //button lancer
    

    @IBAction func LancerButtonPressed(_ sender: Any) {
        /*
        let VC : TabBarViewController = self.storyboard?.instantiateViewController(withIdentifier: "TabBarViewController") as! TabBarViewController
        self.navigationController?.pushViewController(VC, animated: true)
        // self.navigationController?.setNavigationBarHidden(true, animated: false)*/
        //MARK : Setup SSASideMenu*****************+
        
        let sideMenu = SSASideMenu(contentViewController: UINavigationController(rootViewController: InfoGeneralesViewController()), leftMenuViewController: LeftMenuViewController(), rightMenuViewController: RightMenuViewController())
        sideMenu.backgroundImage = UIImage(named:"backgroundmenu.jpg")
        sideMenu.configure(SSASideMenu.MenuViewEffect(fade: true, scale: true, scaleBackground: false))
        sideMenu.configure(SSASideMenu.ContentViewEffect(alpha: 1.0, scale: 0.7))
        sideMenu.configure(SSASideMenu.ContentViewShadow(enabled: true, color: UIColor.black, opacity: 0.6, radius: 6.0))
        sideMenu.delegate = self
        self.navigationController?.pushViewController(sideMenu, animated: true)
    }
}
