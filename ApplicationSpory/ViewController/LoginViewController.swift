//
//  ViewController.swift
//  ApplicationSpory
//
//  Created by macbook pro on 16/04/2018.
//  Copyright © 2018 macbook pro. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController ,UITextFieldDelegate , SSASideMenuDelegate{

    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var mdpTextField: UITextField!
    @IBOutlet weak var seconnecterButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
         self.navigationController?.setNavigationBarHidden(true, animated: false)
      emailTextField.layer.cornerRadius = 20
        mdpTextField.layer.cornerRadius = 20
        seconnecterButton.layer.cornerRadius = 20
        emailTextField.layer.borderColor = UIColor.white.cgColor
        emailTextField.layer.borderWidth = 2 ;
        emailTextField.attributedPlaceholder = NSAttributedString(string:"    Email", attributes:[NSAttributedStringKey.foregroundColor: UIColor.white,NSAttributedStringKey.font :UIFont(name: "Arial", size: 14)!])
       mdpTextField.layer.borderColor = UIColor.white.cgColor
        mdpTextField.layer.borderWidth = 2 ;
        mdpTextField.attributedPlaceholder = NSAttributedString(string:"    Mot de passe", attributes:[NSAttributedStringKey.foregroundColor: UIColor.white,NSAttributedStringKey.font :UIFont(name: "Arial", size: 14)!])
        
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }

//button mot de passe oublie
    
   /* @IBAction func MdpOubliePressed(_ sender: Any) {
       
        let VC : ReinitMdpViewController = self.storyboard?.instantiateViewController(withIdentifier: "ReinitMdpViewController") as! ReinitMdpViewController
        self.navigationController?.pushViewController(VC, animated: true)
    }
    //button rejoigner
    
    @IBAction func RejoigerButtonPressed(_ sender: Any) {
        
        let VC : InscriptionViewController = self.storyboard?.instantiateViewController(withIdentifier: "InscriptionViewController") as! InscriptionViewController
        self.navigationController?.pushViewController(VC, animated: true)
    }
 */
    @IBAction func MdpOublié(_ sender: Any) {
        let VC : ReinitMdpViewController = self.storyboard?.instantiateViewController(withIdentifier: "ReinitMdpViewController") as! ReinitMdpViewController
        self.navigationController?.pushViewController(VC, animated: true)
          self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    ///*****rejoigner button
    
    @IBAction func RejoignezButtonPressed(_ sender: Any) {
        let VC : InscriptionViewController = self.storyboard?.instantiateViewController(withIdentifier: "InscriptionViewController") as! InscriptionViewController
        self.navigationController?.pushViewController(VC, animated: true)
       // self.navigationController?.setNavigationBarHidden(true, animated: false)
        
    }
     // button se connecter
    @IBAction func SeConnecterButton(_ sender: Any) {
        //MARK : Setup SSASideMenu*****************+
        
        let sideMenu = SSASideMenu(contentViewController: UINavigationController(rootViewController: InfoGeneralesViewController()), leftMenuViewController: LeftMenuViewController(), rightMenuViewController: RightMenuViewController())
        sideMenu.backgroundImage = UIImage(named:"backgroundmenu.jpg")
        sideMenu.configure(SSASideMenu.MenuViewEffect(fade: true, scale: true, scaleBackground: false))
        sideMenu.configure(SSASideMenu.ContentViewEffect(alpha: 1.0, scale: 0.7))
        sideMenu.configure(SSASideMenu.ContentViewShadow(enabled: true, color: UIColor.black, opacity: 0.6, radius: 6.0))
        sideMenu.delegate = self
        self.navigationController?.pushViewController(sideMenu, animated: true)


        //  window?.rootViewController = sideMenu
        // window?.makeKeyAndVisible()
       
    }
    
}

