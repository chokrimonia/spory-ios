//
//  LieuxViewController.swift
//  ApplicationSpory
//
//  Created by macbook pro on 25/04/2018.
//  Copyright © 2018 macbook pro. All rights reserved.
//

import UIKit
import MapKit
class LieuxViewController: UIViewController ,UITableViewDataSource,UITableViewDelegate{
var statutsegment = 0
    let LieuImage = ["ptennis","pswimming","pcourse"]
    let NomLieu = ["Park léo Lagrange","Park Jordan ","Maldo piscine"]
    let Lieu = ["Tennis","Natation","Course"]
   
    
    @IBOutlet weak var lieuTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lieuTableView.delegate =  self
        lieuTableView.dataSource = self
    self.navigationController?.setNavigationBarHidden(true, animated: false)
       lieuTableView.register(UINib(nibName: "PartieTableViewCell", bundle: nil), forCellReuseIdentifier: "PartieTableViewCell")
        lieuTableView.register(UINib(nibName: "FilterTableViewCell", bundle: nil), forCellReuseIdentifier: "FilterTableViewCell")
        // Do any additional setup after loading the view.
    }
    //**********trois methodes***********************
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
            return 3
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
            let cell = lieuTableView.dequeueReusableCell(withIdentifier: "PartieTableViewCell", for: indexPath) as! PartieTableViewCell
            cell.partieImage.image = UIImage(named :LieuImage [indexPath.row])
            cell.nompartieLbl.text = NomLieu [indexPath.row]
            cell.partieLbl.text = Lieu [indexPath.row]
            
            return cell
       
        
       
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    
//button menu
    
    @IBAction func MenuButtonPressed(_ sender: Any) {
        sideMenuViewController?._presentLeftMenuViewController()
    }
    
    //Button map
    
    @IBAction func SuivantButtonPressed(_ sender: Any) {
        let VC :MapLieuxViewController = self.storyboard?.instantiateViewController(withIdentifier: "MapLieuxViewController") as! MapLieuxViewController
        self.navigationController?.pushViewController(VC, animated: true)
    }
    //button ajout lieu
    
    @IBAction func AjoutButtonPressed(_ sender: Any) {
        let VC :AjouterLieuViewController = self.storyboard?.instantiateViewController(withIdentifier: "AjouterLieuViewController") as! AjouterLieuViewController
        self.navigationController?.pushViewController(VC, animated: true)
    }
    
}
