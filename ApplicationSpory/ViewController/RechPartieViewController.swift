//
//  RechPartieViewController.swift
//  ApplicationSpory
//
//  Created by macbook pro on 17/04/2018.
//  Copyright © 2018 macbook pro. All rights reserved.
//

import UIKit

class RechPartieViewController: UIViewController ,UICollectionViewDelegate,UICollectionViewDataSource{

    @IBOutlet weak var collectionView: UICollectionView!
    var statusArray : NSMutableArray = ["0","0","0","0","0","0","0","0","0","0"]

    let sports = ["Ellipse","Cycling","Dumbbell","Running","Soccer","Surfing","Swimming","Tennis_Racquet","Walking","Handball"]
    let sportscolorés = ["Ellipseb","Cyclingb","Dumbbellb","Runningb","Soccerb","Surfingb","Swimmingb","Tennis_Racquetb","Walkingb","Handballb"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

       collectionView.delegate = self
        collectionView.dataSource = self
        //print( "Value of index is \(statusArray)")
        /*
        for index in sports {
           // statusArray.insert("0", at: 0)
            statusArray.add("0")

        }
        print( "Value of index is \(statusArray)")
*/
    }

    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        return sports.count
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier:"PartieCollectionViewCell", for: indexPath) as! PartieCollectionViewCell
        if statusArray == ["0","0","0","0","0","0","0","0","0","0"] {
        cell.imageView.image = UIImage(named: sports[indexPath.row])
        }else {
            cell.imageView.image = UIImage(named: sportscolorés[indexPath.row])
        }
        return cell
    }

    public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    
       // collectionView.cellForItem(at: indexPath as IndexPath)?.backgroundColor = UIColor.red
        collectionView.cellForItem(at:indexPath as IndexPath);statusArray.insert("0", at:0)
      
    // statusArray.insert("1", at: 0)
        collectionView.reloadData()
    }
       //buttonback********************
    @IBAction func BackButtonPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated:true)
    }
    
   //button suivant
    
    @IBAction func SuivantButtonPressed(_ sender: Any) {
    
        let VC : RechPartieLancerViewController = self.storyboard?.instantiateViewController(withIdentifier: "RechPartieLancerViewController") as! RechPartieLancerViewController
        self.navigationController?.pushViewController(VC, animated: true)
        // self.navigationController?.setNavigationBarHidden(true, animated: false)
        
    }
 
   
}
