//
//  AmisViewController.swift
//  ApplicationSpory
//
//  Created by macbook pro on 19/04/2018.
//  Copyright © 2018 macbook pro. All rights reserved.
//

import UIKit

class AmisViewController: UIViewController ,UITableViewDataSource,UITableViewDelegate{

    @IBOutlet weak var noterButton: UIButton!
    
    @IBOutlet weak var contacterButton: UIButton!
    @IBOutlet weak var suivreButton: UIButton!
     let personnes = ["female","male"]
    
    let apropos = ["Sexe :","date de naissance :","Pays-Ville :","Email :" ,"Téléphone :","Facebook :","Twitter :","Langages :"]
    let reponses = ["homme","5-6-19987","France-Paris","Pseudo@gmail.com","+339876654:","Nom Prenom","Nom Prenom","Francais-Anglais"]
    @IBOutlet weak var amisTableView: UITableView!
    
    @IBOutlet weak var mySegmetedControl: UISegmentedControl!
    
    override func viewDidLoad() {
        //**********************************************
        view.backgroundColor = UIColor.white
        /*
        title = "Amis"
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Left", style: .plain, target: self, action: #selector(SSASideMenu.presentLeftMenuViewController))*/
         self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        //*************************************************
        suivreButton.layer.cornerRadius = 15
        noterButton .layer.cornerRadius = 15
        contacterButton.layer.cornerRadius = 15
        amisTableView.delegate = self
        amisTableView.dataSource = self
        super.viewDidLoad()
        amisTableView.register(UINib(nibName: "AproposTableViewCell", bundle: nil), forCellReuseIdentifier: "AproposTableViewCell")
        amisTableView.register(UINib(nibName: "PostsTableViewCell", bundle: nil), forCellReuseIdentifier: "PostsTableViewCell")
        // Do any additional setup after loading the view.
    }

    //**********trois methodes***********************
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var returnValue = 0
        switch (mySegmetedControl.selectedSegmentIndex){
        case 0:
            returnValue = 1
        case 1:
            returnValue = 8
            
        case 2:
            break
        case 3:
            break
        default:
            break
            
        }
        return returnValue
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch (mySegmetedControl.selectedSegmentIndex){
        case 0:
            let cell = amisTableView.dequeueReusableCell(withIdentifier: "PostsTableViewCell", for: indexPath) as! PostsTableViewCell
            cell.improfilImageView.image = UIImage(named: personnes [indexPath.row] )
            return cell
        default:
            let cell = amisTableView.dequeueReusableCell(withIdentifier: "AproposTableViewCell", for: indexPath) as! AproposTableViewCell
            
            cell.nomLbl.text = apropos [indexPath.row]
            cell.reponsLbl.text = reponses[indexPath.row]
            
            return cell
            
        }
        
        
        
        
        
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }

    @IBAction func SegmentedControlAction(_ sender: Any) {
        amisTableView.reloadData()
    }
    //buton back
    @IBAction func BackButtonPressed(_ sender: Any) {
         sideMenuViewController?._presentLeftMenuViewController()
    }
    
}
