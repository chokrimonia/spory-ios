//
//  ContactViewController.swift
//  ApplicationSpory
//
//  Created by macbook pro on 24/04/2018.
//  Copyright © 2018 macbook pro. All rights reserved.
//

import UIKit

class ContactViewController: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var envoyerButoon: UIButton!
    @IBOutlet weak var sujetTextField: UITextField!
    
    @IBOutlet weak var messageTextField: UITextField!
    override func viewDidLoad() {
        //**********************************************
        view.backgroundColor = UIColor.white
        /*
        title = "Contact"
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Left", style: .plain, target: self, action: #selector(SSASideMenu.presentLeftMenuViewController))*/
         self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        //*************************************************
        super.viewDidLoad()
sujetTextField.layer.cornerRadius = 20
        messageTextField.layer.cornerRadius = 20
        envoyerButoon.layer.cornerRadius = 20
        // Do any additional setup after loading the view.
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    @IBAction func BackButtonPressed(_ sender: Any) {
         sideMenuViewController?._presentLeftMenuViewController()
    }
    
  

}
