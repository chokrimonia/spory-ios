//
//  ProfilViewController.swift
//  ApplicationSpory
//
//  Created by macbook pro on 19/04/2018.
//  Copyright © 2018 macbook pro. All rights reserved.
//

import UIKit

class ProfilViewController: UIViewController ,UITableViewDelegate,UITableViewDataSource{
    let apropos = ["Sexe :","date de naissance :","Pays-Ville :","Email :","Mot de passe :","Téléphone :","Facebook :","Twitter :","Langages :"]
    let reponses = ["homme","5-6-19987","France-Paris","Pseudo@gmail.com","*****","+339876654:","Nom Prenom","Nom Prenom","Francais-Anglais"]
    
    @IBOutlet weak var mySgmentedControl: UISegmentedControl!
    
    let personnes = ["female","male"]
    @IBOutlet weak var profilTableView: UITableView!
    
    override func viewDidLoad() {
        //**********************************************
        view.backgroundColor = UIColor.white
        /*
        title = "Mon Profil"
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Left", style: .plain, target: self, action: #selector(SSASideMenu.presentLeftMenuViewController))
 */
         self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        //*************************************************
       profilTableView.delegate = self
       profilTableView.dataSource = self
        
    profilTableView.register(UINib(nibName: "AproposTableViewCell", bundle: nil), forCellReuseIdentifier: "AproposTableViewCell")
         profilTableView.register(UINib(nibName: "PostsTableViewCell", bundle: nil), forCellReuseIdentifier: "PostsTableViewCell")
        super.viewDidLoad()
        
       
        // Do any additional setup after loading the view.

    }
   
    //**********trois methodes***********************
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var returnValue = 0
        switch (mySgmentedControl.selectedSegmentIndex){
        case 0:
            returnValue = 1
        case 1:
           returnValue = 9
            
        case 2:
            break
        case 3:
            break
        default:
            break
            
        }
        return returnValue
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      
        switch (mySgmentedControl.selectedSegmentIndex){
        case 0:
            let cell = profilTableView.dequeueReusableCell(withIdentifier: "PostsTableViewCell", for: indexPath) as! PostsTableViewCell
          cell.improfilImageView.image = UIImage(named: personnes [indexPath.row] )
            return cell
        default:
             let cell = profilTableView.dequeueReusableCell(withIdentifier: "AproposTableViewCell", for: indexPath) as! AproposTableViewCell
             
            cell.nomLbl.text = apropos [indexPath.row]
             cell.reponsLbl.text = reponses[indexPath.row]
            
                return cell
                
             }
        
            
            
        
     
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
 //fonction segment
    
    

    @IBAction func SegmentedControlAction(_ sender: Any) {
        profilTableView.reloadData()
    }
    // button back
 
    
    @IBAction func BackButtonPressed(_ sender: Any) {
        sideMenuViewController?._presentLeftMenuViewController()
    }
    
}
