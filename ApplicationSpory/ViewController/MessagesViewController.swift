//
//  MessagesViewController.swift
//  ApplicationSpory
//
//  Created by macbook pro on 19/04/2018.
//  Copyright © 2018 macbook pro. All rights reserved.
//

import UIKit

class MessagesViewController: UIViewController ,UITableViewDelegate,UITableViewDataSource{
    @IBOutlet weak var mySegmentedControl: UISegmentedControl!
    
    @IBOutlet weak var messagesTableView: UITableView!
    let  nompersonnes = ["Pseudo1","Pseudo2","Pseudo3","Pseudo4","Pseudo5","Pseudo6"]
    let personnes = ["female","male","female","male","female","male"]
    let groupes = ["rencontre4","rencontre2","rencontre2","rencontre3","rencontre2","rencontre4"]
    let nomgroupes = ["GroupesTennis","GroupesBasket","GroupesMarche","GroupesFooting","GroupesCycling","GroupesSurfing"]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //**********************************************
        view.backgroundColor = UIColor.white
        /*
        title = "Messages"
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Left", style: .plain, target: self, action: #selector(SSASideMenu.presentLeftMenuViewController))*/
         self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        //*************************************************
        messagesTableView.delegate = self
        messagesTableView.dataSource = self
        
        messagesTableView.register(UINib(nibName: "PersonnesTableViewCell", bundle: nil), forCellReuseIdentifier: "PersonnesTableViewCell")
    }
    //**********trois methodes***********************
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var returnValue = 0
        switch (mySegmentedControl.selectedSegmentIndex){
        case 0:
            returnValue = personnes.count
        case 1:
            returnValue = groupes.count
        case 2:
            break
        default:
            break
            
        }
        return returnValue
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = messagesTableView.dequeueReusableCell(withIdentifier: "PersonnesTableViewCell", for: indexPath) as! PersonnesTableViewCell
        
        
        switch (mySegmentedControl.selectedSegmentIndex){
        case 0:
            cell.personnesImage.image = UIImage(named: personnes [indexPath.row] )
            cell.nomLbl.text = nompersonnes[indexPath.row]
           
        case 1:
            cell.personnesImage.image = UIImage(named: groupes [indexPath.row] )
            cell.nomLbl.text = nomgroupes[indexPath.row]
       
        default:
            break
            
        }
        return cell
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
     // MARK: - select cell
     
    
         func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

            let vc = self.storyboard?.instantiateViewController(
                withIdentifier: "DetMesPersViewController") as! DetMesPersViewController
            switch (mySegmentedControl.selectedSegmentIndex){
            case 0:
                vc.isPersone = true

            default:
                 vc.isPersone = false
            }
            

            navigationController?.pushViewController(vc, animated: true)
    }
 
    ///************************************
    
    @IBAction func segmentedControlAction(_ sender: Any) {
        messagesTableView.reloadData()
    }
    
  //back message
    @IBAction func BackButtonPressed(_ sender: Any) {
         sideMenuViewController?._presentLeftMenuViewController()
    }
    

}
