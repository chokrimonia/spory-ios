//
//  ReinitMdpViewController.swift
//  ApplicationSpory
//
//  Created by macbook pro on 17/04/2018.
//  Copyright © 2018 macbook pro. All rights reserved.
//

import UIKit

class ReinitMdpViewController: UIViewController ,UITextFieldDelegate{

    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var envoyerButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        emailTextField.layer.cornerRadius = 20
        envoyerButton.layer.cornerRadius = 20
       emailTextField.attributedPlaceholder = NSAttributedString(string:"     Pseudo@gmail.com", attributes:[NSAttributedStringKey.foregroundColor: UIColor.gray,NSAttributedStringKey.font :UIFont(name: "Arial", size: 14)!])
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
   
  //button back login
    
    @IBAction func BackButton(_ sender: Any) {
self.navigationController?.popViewController(animated:true)
    }
    
    
}
