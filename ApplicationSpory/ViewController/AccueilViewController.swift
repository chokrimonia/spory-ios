//
//  AccueilViewController.swift
//  ApplicationSpory
//
//  Created by macbook pro on 19/04/2018.
//  Copyright © 2018 macbook pro. All rights reserved.
//

import UIKit
import MapKit
class AccueilViewController: UIViewController ,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate{
    @IBOutlet weak var ajouterButton: UIButton!
    
    
    let PartieImage = ["ptennis","pswimming","pcourse"]
    let NomPartie = ["Partie de Tennis","Garder votre coprs au frais ","Partie rapide "]
    let Partie = ["Tennis","Natation","Course"]
   
    @IBOutlet weak var viewSearch: UIView!
    @IBOutlet weak var mapView: MKMapView!
    
    var statutsegment = 0
     let personnes = ["female","male","female","male","female","male"]
    let parties = ["partietennis"]
    @IBOutlet weak var mySgmentedControl: UISegmentedControl!
    
    @IBOutlet weak var accueilTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        accueilTableView.isHidden = true
        mapView.isHidden = false
        viewSearch.isHidden = false
         ajouterButton.isHidden = true
        //**********************************************
        view.backgroundColor = UIColor.white
        /*
        title = "Accueil"
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Left", style: .plain, target: self, action: #selector(SSASideMenu.presentLeftMenuViewController))
 */
        self.navigationController?.setNavigationBarHidden(true, animated: false)
       
        //Mark *************************************************
        
       accueilTableView.delegate = self
       accueilTableView.dataSource = self
        //accueilTableView.register(UINib(nibName: "MapTableViewCell", bundle: nil), forCellReuseIdentifier: "MapTableViewCell")
        accueilTableView.register(UINib(nibName: "PostsTableViewCell", bundle: nil), forCellReuseIdentifier: "PostsTableViewCell")
         accueilTableView.register(UINib(nibName: "PartieTableViewCell", bundle: nil), forCellReuseIdentifier: "PartieTableViewCell")
          accueilTableView.register(UINib(nibName: "FilterTableViewCell", bundle: nil), forCellReuseIdentifier: "FilterTableViewCell")
       
       
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    //********** Mark trois methodes***********************
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if (mySgmentedControl.selectedSegmentIndex) == 0 {
       
             return 1
        }
      
          else if (mySgmentedControl.selectedSegmentIndex) == 1 {
            return  1
            }
        else  if (mySgmentedControl.selectedSegmentIndex) == 2 {
            return 3
        }
        else{
            
            return 1
        
    }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            if statutsegment == 1 {
             let cell = accueilTableView.dequeueReusableCell(withIdentifier: "PostsTableViewCell", for: indexPath) as! PostsTableViewCell
            cell.improfilImageView.image = UIImage(named: personnes [indexPath.row] )
            cell.impartieImageView.image = UIImage(named: parties [indexPath.row])
         
          return cell
        }
        else if statutsegment == 2  {
               let cell = accueilTableView.dequeueReusableCell(withIdentifier: "PartieTableViewCell", for: indexPath) as! PartieTableViewCell
                cell.partieImage.image = UIImage(named :PartieImage [indexPath.row])
                cell.nompartieLbl.text = NomPartie [indexPath.row]
                cell.partieLbl.text = Partie [indexPath.row]
            
     return cell
   
            }else {
                let cell = accueilTableView.dequeueReusableCell(withIdentifier: "FilterTableViewCell", for: indexPath) as! FilterTableViewCell
            
                return cell
        }
    
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if (mySgmentedControl.selectedSegmentIndex) == 0 {
           
            return 100
        }
           
        else if (mySgmentedControl.selectedSegmentIndex) == 1 {
            return  100
        }
        else  if (mySgmentedControl.selectedSegmentIndex) == 2 {
            return 150
        }
        else{
            
            return 1
            
        }
    }


// Mark fonction segment
    
   
  
    @IBAction func SegmentedControlAction(_ sender: Any) {
         if (mySgmentedControl.selectedSegmentIndex) == 0 {
               accueilTableView.isHidden = true
               mapView.isHidden = false
               viewSearch.isHidden = false
            ajouterButton.isHidden = true
           statutsegment = 0
        }
       else if (mySgmentedControl.selectedSegmentIndex) == 1{
                  accueilTableView.isHidden = false
                  mapView.isHidden = true
                  viewSearch.isHidden = true
             ajouterButton.isHidden = true
                   accueilTableView.reloadData()
            statutsegment = 1
        }
         else if (mySgmentedControl.selectedSegmentIndex) == 2 {
            accueilTableView.isHidden = false
                  mapView.isHidden = true
                  viewSearch.isHidden = true
             ajouterButton.isHidden = false
                  accueilTableView.reloadData()
            statutsegment = 2
        }
        else if (mySgmentedControl.selectedSegmentIndex) == 3 {
                   accueilTableView.isHidden = false
                   mapView.isHidden = true
                   viewSearch.isHidden = true
             ajouterButton.isHidden = true
                   accueilTableView.reloadData()
      statutsegment = 3
}
    }
  // Mark button menu
    
    @IBAction func MenuButtonPressed(_ sender: Any) {
         sideMenuViewController?._presentLeftMenuViewController()

 
    }
}

