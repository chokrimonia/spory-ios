//
//  MesRencontresViewController.swift
//  ApplicationSpory
//
//  Created by macbook pro on 24/04/2018.
//  Copyright © 2018 macbook pro. All rights reserved.
//

import UIKit

class MesRencontresViewController: UIViewController ,UITableViewDelegate,UITableViewDataSource{
    @IBOutlet weak var mySegmentedControl: UISegmentedControl!
    
    @IBOutlet weak var rencontreTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        //**********************************************
        view.backgroundColor = UIColor.white
        /*
        title = "Mes Rencontres"
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Left", style: .plain, target: self, action: #selector(SSASideMenu.presentLeftMenuViewController))*/
         self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        //*************************************************
        rencontreTableView.delegate = self
       rencontreTableView.dataSource = self
        
        rencontreTableView.register(UINib(nibName: "RencontresTableViewCell", bundle: nil), forCellReuseIdentifier: "RencontresTableViewCell")
        // Do any additional setup after loading the view.
    }

    //**********trois methodes***********************
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var returnValue = 0
        switch (mySegmentedControl.selectedSegmentIndex){
        case 0:
            returnValue = 1
        default :
            returnValue = 1
       
        }
        
        
        return returnValue
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        
        
               switch (mySegmentedControl.selectedSegmentIndex){
              case 0:
                  let cell = rencontreTableView.dequeueReusableCell(withIdentifier: "RencontresTableViewCell", for: indexPath) as! RencontresTableViewCell
         return cell
               default:
                let cell = rencontreTableView.dequeueReusableCell(withIdentifier: "RencontresTableViewCell", for: indexPath) as! RencontresTableViewCell
                return cell
                }
       
       
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    

    @IBAction func SegmentedControlAction(_ sender: Any) {
        rencontreTableView.reloadData()
    }
    
//back mes rencontres
    
    @IBAction func BackButtonPressed(_ sender: Any) {
         sideMenuViewController?._presentLeftMenuViewController()
    }
}
