//
//  LeftMenuViewController.swift
//  SSASideMenuExample
//
//  Created by Sebastian Andersen on 20/10/14.
//  Copyright (c) 2015 Sebastian Andersen. All rights reserved.
//

import Foundation
import UIKit

class LeftMenuViewController: UIViewController {
    
    let titles: [String] = ["Accueil", " Mon Profil", "Calendrier","Mes Rencontres","Lieux","Amis","Messages","Notifications","Configuration", "Contact","Information Generales ","Log Out"]
    // let titles: [String] = ["Home", "Calendar", "Profile", "Settings", "Log Out"]
    
    let images: [String] = ["IconHome", "IconProfile", "IconCalendar","mesrencontres", "lieux","amis","messages","notifications","configuration", "contact","informationsgénérales","déconnexion"]
    
    lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        tableView.frame = CGRect(x: 20, y: (self.view.frame.size.height - 54 * 12) / 2.0, width: self.view.frame.size.width, height: 54 * 12)
        tableView.autoresizingMask = [.flexibleTopMargin, .flexibleBottomMargin, .flexibleWidth]
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        tableView.isOpaque = false
        tableView.backgroundColor = UIColor.clear
        tableView.backgroundView = nil
        tableView.bounces = false
        return tableView
    }()

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        


        view.backgroundColor = UIColor.clear
        view.addSubview(tableView)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
}


// MARK : TableViewDataSource & Delegate Methods

extension LeftMenuViewController: UITableViewDelegate, UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titles.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 54
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) 
   
  
        
        cell.backgroundColor = UIColor.clear
        cell.textLabel?.font = UIFont(name: "HelveticaNeue", size: 15)
        cell.textLabel?.textColor = UIColor.white
        cell.textLabel?.text  = titles[indexPath.row]
        cell.selectionStyle = .none
        cell.imageView?.image = UIImage(named: images[indexPath.row])
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
     
        switch indexPath.row {
        case 0:
            
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            
            let AccueilViewController = storyBoard.instantiateViewController(withIdentifier: "AccueilViewController") as! AccueilViewController
            //self.present(InfoGeneralesViewController, animated:true, completion:nil)
            
            sideMenuViewController?.contentViewController = UINavigationController(rootViewController: AccueilViewController)
            sideMenuViewController?.hideMenuViewController()
            break
        case 1:
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            
            let ProfilViewController = storyBoard.instantiateViewController(withIdentifier: "ProfilViewController") as! ProfilViewController
            //self.present(InfoGeneralesViewController, animated:true, completion:nil)
            
            sideMenuViewController?.contentViewController = UINavigationController(rootViewController: ProfilViewController)
            sideMenuViewController?.hideMenuViewController()
            break
            
            //********************
        case 2:
            sideMenuViewController?.contentViewController = UINavigationController(rootViewController: SecondViewController())
            sideMenuViewController?.hideMenuViewController()
            break
          
            //*************************
        case 3:
            
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            
            let MesRencontresViewController = storyBoard.instantiateViewController(withIdentifier: "MesRencontresViewController") as! MesRencontresViewController
            //self.present(InfoGeneralesViewController, animated:true, completion:nil)
            
            sideMenuViewController?.contentViewController = UINavigationController(rootViewController: MesRencontresViewController)
            sideMenuViewController?.hideMenuViewController()
            break
            ///********************
        case 4:
            
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            
            let LieuxViewController = storyBoard.instantiateViewController(withIdentifier: "LieuxViewController") as! LieuxViewController
            //self.present(InfoGeneralesViewController, animated:true, completion:nil)
            
            sideMenuViewController?.contentViewController = UINavigationController(rootViewController: LieuxViewController)
            sideMenuViewController?.hideMenuViewController()
            break
            //**********************
        case 5:
            
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            
            let AmisViewController = storyBoard.instantiateViewController(withIdentifier: "AmisViewController") as! AmisViewController
            //self.present(InfoGeneralesViewController, animated:true, completion:nil)
            
            sideMenuViewController?.contentViewController = UINavigationController(rootViewController: AmisViewController)
            sideMenuViewController?.hideMenuViewController()
            break
            //
        case 6:
            
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            
            let MessagesViewController = storyBoard.instantiateViewController(withIdentifier: "MessagesViewController") as! MessagesViewController
            //self.present(InfoGeneralesViewController, animated:true, completion:nil)
            
            sideMenuViewController?.contentViewController = UINavigationController(rootViewController: MessagesViewController)
            sideMenuViewController?.hideMenuViewController()
            break
            //*****************+
        case 7:
            
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            
            let NotificationsViewController = storyBoard.instantiateViewController(withIdentifier: "NotificationsViewController") as! NotificationsViewController
            //self.present(InfoGeneralesViewController, animated:true, completion:nil)
            
            sideMenuViewController?.contentViewController = UINavigationController(rootViewController: NotificationsViewController)
            sideMenuViewController?.hideMenuViewController()
            break
            //******************
        case 8:
            
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            
            let ConfigurationViewController = storyBoard.instantiateViewController(withIdentifier: "ConfigurationViewController") as! ConfigurationViewController
            //self.present(InfoGeneralesViewController, animated:true, completion:nil)
            
            sideMenuViewController?.contentViewController = UINavigationController(rootViewController: ConfigurationViewController)
            sideMenuViewController?.hideMenuViewController()
            break
            //**********************
        case 9:
            
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            
            let ContactViewController = storyBoard.instantiateViewController(withIdentifier: "ContactViewController") as! ContactViewController
            //self.present(InfoGeneralesViewController, animated:true, completion:nil)
            
            sideMenuViewController?.contentViewController = UINavigationController(rootViewController: ContactViewController)
            sideMenuViewController?.hideMenuViewController()
            break
            //********************
        case 10:
            
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            
            let InfoGeneralesViewController = storyBoard.instantiateViewController(withIdentifier: "InfoGeneralesViewController") as! InfoGeneralesViewController
            //self.present(InfoGeneralesViewController, animated:true, completion:nil)
            
            sideMenuViewController?.contentViewController = UINavigationController(rootViewController: InfoGeneralesViewController)
            sideMenuViewController?.hideMenuViewController()
            break
            //***************
        case 11:
            
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            
            let LoginViewController = storyBoard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            //self.present(InfoGeneralesViewController, animated:true, completion:nil)
            
            sideMenuViewController?.contentViewController = UINavigationController(rootViewController: LoginViewController)
            sideMenuViewController?.hideMenuViewController()
            break
            //***********
        default:
            break
        }
        
        
    }
    
}
    
